/**
 * API URLS
 */
export const urls = {
  login: "/auth/admin/login",
  user: "/admin/users",
  serviceProviders: "/admin/service-providers",
  serviceProvidersDetails: "/admin/service-provider/detail",
  services: "/admin/services",
  serviceDetails: "/admin/service/detail",
  addons: "/admin/addons",
  register: "/auth/register",
  application: "/application",
};
