import { urls } from './url';

export const environment = {
  production: true,
  host: '',
  ...urls
};
