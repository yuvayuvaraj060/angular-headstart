export const settingConfig = {
  currentUseType: [
    {
      key: "Residential",
      value: "Residential"
    },
    {
      key: "Commerical",
      value: "Commerical"
    },
    {
      key: "Mixed Use",
      value: "Mixed Use"
    },
  ],
  currentStatus: [
    {
      key: "Vacant",
      value: "Vacant"
    },
    {
      key: "Occupied",
      value: "Occupied"
    }
  ],
  role: [
    {
      key: "Property Owner",
      value: "Property Owner"
    },
    {
      key: "Property Owners Agent",
      value: "Property Owners Agent"
    },
    {
      key: "Buyer",
      value: "Buyer (Auction or Sheriff Sale or Bank owned)"
    }
  ],
  ownerType: [
    {
      key: "Single Individual",
      value: "Single Individual"
    },
    {
      key: "Business Entity/Trust",
      value: "Business Entity/Trust"
    },
    {
      key: "Bank",
      value: "Bank"
    }
  ],
  buyerType: [
    {
      key: "Individual",
      value: "Individual"
    },
    {
      key: "Business",
      value: "Business"
    }
  ],
  businessType: [
    {
      key: "Corporation",
      value: "Corporation"
    },
    {
      key: "Partnership",
      value: "Partnership"
    },
    {
      key: "LLC",
      value: "LLC"
    }
  ],
  pointofContact: [
    {
      key: "Applicant",
      value: "Applicant"
    },
    {
      key: "Property Owner",
      value: "Property Owner"
    },
    {
      key: "Someone else",
      value: "Someone else"
    }
  ],

}
