import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class UsersService {
  constructor(private http: HttpClient) {}

  users(pageNumber: number = 1): Observable<UserBlob> {
    const path = `${environment.user}?page=${pageNumber}`;

    return this.http.get<UserBlob>(path);
  }
}

export type UserType = {
  avatar: string;
  countryCode: string;
  phone: string;
  resetPasswordToken: boolean;
  resetPasswordAt: string;
  resetPasswordTokenVerified: boolean;
  otp: string;
  otpGeneratedAt: string;
  isOtpVerified: boolean;
  status: number;
  type: number;
  stripeCustomerId: string;
  stripeConnectAccountId: boolean;
  stripeConnectAccountRequirement: [];
  _id: string;
  email: string;
  password: string;
  name: string;
  tokens: [
    {
      _id: string;
      access: string;
      token: string;
    }
  ];
  createdAt: string;
  updatedAt: string;
  __v: number;
};

export type UserBlob = {
  status: string;
  response: UserResponseType;
};

export type UserResponseType = {
  perPage: number;
  currentPage: number;
  totalPages: number;
  totalCount: number;
  data: UserType[];
};
