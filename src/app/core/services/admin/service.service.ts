import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class ServiceService {
  constructor(private http: HttpClient) {}

  servicesId(pageNumber: number = 1): Observable<servicesId> {
    const path = `${environment.services}?page=${pageNumber}`;
    return this.http.get<any>(path);
  }

  serviceDetails(id: string): Observable<any> {
    const path = `${environment.serviceDetails}/${id}`;
    return this.http.get(path);
  }
}

export type servicesId = {
  status: "success";
  response: {
    perPage: 20;
    currentPage: 1;
    totalPages: 2;
    totalCount: 35;
    data: any;
  };
};
