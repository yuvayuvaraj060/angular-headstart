import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class AddonsService {
  constructor(private http: HttpClient) {}

  addons(
    pageNumber: number = 1,
    searchParams?: string
  ): Observable<AddonsResponse> {
    let path = `${environment.addons}?page=${pageNumber}`;
    if (searchParams) {
      path += `?search=${searchParams}`;
    }

    return this.http.get<AddonsResponse>(path);
  }
}

export type AddonsResponse = {
  status: "success";
  response: {
    perPage: 20;
    currentPage: 1;
    totalPages: 2;
    totalCount: 33;
    data: AddonsType[];
  };
};

export type AddonsType = {
  service: "60d9531fe30117626cee4d66";
  name: "dfdswf";
  price: 123;
  description: "fgvdsfgsdfdsf";
  _id: "60d9531fe30117626cee4d67";
  type: 1;
  createdAt: "2021-06-28T04:42:07.202Z";
  updatedAt: "2021-06-28T04:42:07.202Z";
  __v: 0;
};
