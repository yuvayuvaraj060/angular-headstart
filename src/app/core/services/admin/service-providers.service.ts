import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class ServiceProvidersService {
  constructor(private http: HttpClient) {}

  serviceProviders(pageNumber: number = 1): Observable<ServiceProviderBlob> {
    const path = `${environment.serviceProviders}?page=${pageNumber}`;

    return this.http.get<ServiceProviderBlob>(path);
  }

  serviceProviderDetails(id: string): Observable<DetailsProvider> {
    const path = `${environment.serviceProvidersDetails}/${id}`;

    return this.http.get<DetailsProvider>(path);
  }
}

export type ServiceProviderType = {
  avatar: string;
  countryCode: string;
  phone: string;
  resetPasswordToken: boolean;
  resetPasswordAt: string;
  resetPasswordTokenVerified: boolean;
  otp: string;
  otpGeneratedAt: string;
  isOtpVerified: boolean;
  status: number;
  type: number;
  stripeCustomerId: boolean;
  stripeConnectAccountId: string;
  stripeConnectAccountRequirement: [
    {
      current_deadline: boolean;
      currently_due: string[];
      disabled_reason: string;
      errors: [];
      eventually_due: string[];
      past_due: string[];
      pending_verification: [];
    }
  ];
  _id: string;
  email: string;
  password: string;
  name: string;
  tokens: [
    {
      _id: string;
      access: string;
      token: string;
    }
  ];
  createdAt: string;
  updatedAt: string;
  __v: 1;
};

export type ServiceProviderBlob = {
  status: string;
  response: ServiceProviderResponseType;
};

export type ServiceProviderResponseType = {
  perPage: number;
  currentPage: number;
  totalPages: number;
  totalCount: number;
  data: ServiceProviderType[];
};

export type DetailsProvider = {
  status: "success";
  response: {
    baseLocation: {
      coordinates: [144.99837297946215, -37.782507415346565];
      type: "Point";
    };
    serviceProvider: {
      avatar: "avatar-1620816067297.jpg";
      countryCode: "61";
      phone: "(358) 899-9999";
      resetPasswordToken: null;
      resetPasswordAt: "2021-04-30T12:17:45.913Z";
      resetPasswordTokenVerified: false;
      otp: "9509";
      otpGeneratedAt: "2021-05-12T10:35:31.499Z";
      isOtpVerified: true;
      status: 1;
      type: 2;
      stripeCustomerId: null;
      stripeConnectAccountId: "acct_1IqFRTR9ThjIreSo";
      stripeConnectAccountRequirement: [
        {
          current_deadline: null;
          currently_due: ["external_account"];
          disabled_reason: "requirements.past_due";
          errors: [];
          eventually_due: ["external_account"];
          past_due: ["external_account"];
          pending_verification: [];
        }
      ];
      _id: "609baf73e30117626cee4c6d";
      email: "anshu@washer12.com";
      password: "$2b$10$rE7rWDWNZpbJ7tilyo7DxueKX6jzVw/tL/ggyz9BFUsdGtQ7us3Zi";
      name: "Anshu Washer";
      tokens: [
        {
          _id: "609baf73e30117626cee4c6e";
          access: "auth";
          token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDliYWY3M2UzMDExNzYyNmNlZTRjNmQiLCJhY2Nlc3MiOiJhdXRoIiwiZXhwIjoxNjIzNDA3NzMxLCJpYXQiOjE2MjA4MTU3MzF9.XdCngy-nzpIAE_QhHKglxwJg4oYbruQxTdQmCwK4q6s";
        }
      ];
      createdAt: "2021-05-12T10:35:31.501Z";
      updatedAt: "2021-05-12T10:41:08.780Z";
      __v: 1;
    };
    driverLicenseIDNumber: "67827267828";
    driverLicenseFront: "driverLicenseFront-1620815801119.jpg";
    driverLicenseBack: "driverLicenseBack-1620815801548.jpg";
    photoCardIDNumber: null;
    photoCardFront: null;
    photoCardBack: null;
    houseFlatBlock: "52";
    landmark: "abcd";
    zipCode: "3070";
    city: "Northcote";
    state: "VIC";
    dob: "1967-5-10";
    address: "52 Cunningham St, Northcote VIC 3070, Australia";
    ssn: "123456789";
    _id: "609bafb9e30117626cee4c70";
    createdAt: "2021-05-12T10:36:41.627Z";
    updatedAt: "2021-05-12T10:41:07.311Z";
    __v: 0;
  };
};
