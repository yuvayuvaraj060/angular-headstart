import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddonsComponent } from "./addons/addons.component";
import { ServiceProvidersComponent } from "./service-providers/service-providers.component";
import { ServicesComponent } from "./services/services.component";
import { UsersComponent } from "./users/users.component";

const routes: Routes = [
  {
    path: "",
    component: UsersComponent,
  },
  {
    path: "service-providers",
    component: ServiceProvidersComponent,
  },
  {
    path: "services",
    component: ServicesComponent,
  },
  {
    path: "addons",
    component: AddonsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
  static component = [
    UsersComponent,
    ServiceProvidersComponent,
    ServicesComponent,
    AddonsComponent,
  ];
}
