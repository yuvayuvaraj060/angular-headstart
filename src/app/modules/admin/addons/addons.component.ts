import { Component, OnInit } from "@angular/core";
import { AddonsResponse, AddonsService } from "src/app/core/services/admin";

@Component({
  selector: "app-addons",
  templateUrl: "./addons.component.html",
  styleUrls: ["./addons.component.css"],
})
export class AddonsComponent implements OnInit {
  addons: AddonsResponse;
  currentPage: number;
  pageNumber: number[] = [];
  constructor(private addonsServices: AddonsService) {}

  ngOnInit() {
    this.getAddons();
  }

  getAddons() {
    this.addonsServices.addons().subscribe((addonsRes) => {
      this.addons = addonsRes;
      this.currentPage = 1;
      this.makePageNumber();
    });
  }
  getNextPageAddons(pageNumber: number) {
    this.addonsServices.addons(pageNumber).subscribe((addonsRes) => {
      this.addons = addonsRes;
      this.makePageNumber();
      this.currentPage = pageNumber;
    });
  }

  makePageNumber() {
    if (this.addons) {
      this.pageNumber = [];
      for (let num = 1; num <= this.addons.response.totalPages; num++) {
        this.pageNumber.push(num);
      }
    }
  }

  nextPage() {
    if (this.addons.response.totalPages > this.currentPage) {
      this.currentPage = this.currentPage + 1;
      this.getNextPageAddons(this.currentPage);
    }
  }
  previousPage() {
    if (this.addons.response.totalPages > this.currentPage) {
      this.currentPage = this.currentPage - 1;
      this.getNextPageAddons(this.currentPage);
    }
  }
}
