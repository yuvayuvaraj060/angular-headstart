import { Component, OnInit } from "@angular/core";
import { ServiceService, servicesId } from "src/app/core/services/admin";

@Component({
  selector: "app-services",
  templateUrl: "./services.component.html",
  styleUrls: ["./services.component.css"],
})
export class ServicesComponent implements OnInit {
  constructor(private servicesServices: ServiceService) {}
  servicesID: servicesId;
  serviceDetails = [];
  currentPage: number;
  pageNumber: number[] = [];
  ngOnInit() {
    this.getServicesId();
  }

  getServicesId() {
    this.servicesServices.servicesId().subscribe((response) => {
      const ids = response.response.data.map((service) => service._id);
      const responses = { ...response.response, data: ids };
      this.servicesID = { ...response, response: responses };
      this.getServiceDetails();
      this.makePageNumber();
      this.currentPage = 1;
    });
  }

  getNextPageServicesId(pageNumber: number) {
    this.servicesServices.servicesId(pageNumber).subscribe((response) => {
      const ids = response.response.data.map((service) => service._id);
      const responses = { ...response.response, data: ids };
      this.servicesID = { ...response, response: responses };
      this.getServiceDetails();
      this.makePageNumber();
      this.currentPage = pageNumber;
    });
  }

  getServiceDetails() {
    if (this.servicesID) {
      this.servicesID.response.data.map((id) => {
        this.servicesServices.serviceDetails(id).subscribe((serviceDetails) => {
          this.serviceDetails.push(serviceDetails);
        });
      });
    }
  }

  makePageNumber() {
    if (this.servicesID) {
      this.pageNumber = [];
      for (let num = 1; num <= this.servicesID.response.totalPages; num++) {
        this.pageNumber.push(num);
      }
    }
  }

  nextPage() {
    if (this.servicesID.response.totalPages > this.currentPage) {
      this.currentPage = this.currentPage + 1;
      this.getNextPageServicesId(this.currentPage);
    }
  }
  previousPage() {
    if (this.servicesID.response.totalPages > this.currentPage) {
      this.currentPage = this.currentPage - 1;
      this.getNextPageServicesId(this.currentPage);
    }
  }
}
