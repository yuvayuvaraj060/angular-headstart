import { Component, OnInit } from "@angular/core";
import {
  UsersService,
  UserResponseType,
} from "src/app/core/services/admin/index";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"],
})
export class UsersComponent implements OnInit {
  users: UserResponseType;
  currentPage: number;
  pageNumber: number[] = [];
  constructor(private usersServices: UsersService) {}

  ngOnInit() {
    this.getInitialUsers();
  }

  getInitialUsers() {
    this.usersServices.users().subscribe((userBlob) => {
      this.users = userBlob.response;
      this.currentPage = 1;
      this.makePageNumber();
    });
  }

  getNextPageUsers(pageNumber: number) {
    this.usersServices.users(pageNumber).subscribe((userBlob) => {
      this.users = userBlob.response;
      this.currentPage = pageNumber;
    });
  }

  makePageNumber() {
    if (this.users) {
      for (let num = 1; num <= this.users.totalPages; num++) {
        console.log(num);
        this.pageNumber.push(num);
      }
    }
  }
}
