import { Component, OnInit } from "@angular/core";
import {
  ServiceProvidersService,
  ServiceProviderResponseType,
  DetailsProvider,
} from "src/app/core/services/admin/index";

@Component({
  selector: "app-service-providers",
  templateUrl: "./service-providers.component.html",
  styleUrls: ["./service-providers.component.css"],
})
export class ServiceProvidersComponent implements OnInit {
  serviceProviderServices: ServiceProviderResponseType;
  details: DetailsProvider[] = [];
  currentPage: number;
  pageNumber: number[] = [];

  constructor(private serviceProServices: ServiceProvidersService) {}

  ngOnInit() {
    this.getServiceProvides();
  }

  getServiceProvides() {
    this.serviceProServices.serviceProviders().subscribe((serviceProviders) => {
      this.serviceProviderServices = serviceProviders.response;
      this.makePageNumber();
      serviceProviders.response.data.map((data) => {
        this.getServiceProvideDetails(data._id);
      });
      this.currentPage = 1;
    });
  }

  getNextPageServiceProvides(pageNumber: number) {
    this.serviceProServices
      .serviceProviders(pageNumber)
      .subscribe((serviceProviders) => {
        this.serviceProviderServices = serviceProviders.response;
        this.makePageNumber();
        serviceProviders.response.data.map((data) => {
          this.getServiceProvideDetails(data._id);
        });
        this.currentPage = pageNumber;
      });
  }

  getServiceProvideDetails(id: string) {
    this.serviceProServices.serviceProviderDetails(id).subscribe((details) => {
      this.details.push(details);
      console.log(details);
    });
  }
  makePageNumber() {
    if (this.serviceProviderServices) {
      this.pageNumber = [];
      for (let num = 1; num <= this.serviceProviderServices.totalPages; num++) {
        console.log(num);
        this.pageNumber.push(num);
      }
    }
  }

  nextPage() {
    if (this.serviceProviderServices.totalPages > this.currentPage) {
      this.currentPage = this.currentPage + 1;
      this.getNextPageServiceProvides(this.currentPage);
    }
  }
  previousPage() {
    if (this.serviceProviderServices.totalPages > this.currentPage) {
      this.currentPage = this.currentPage - 1;
      this.getNextPageServiceProvides(this.currentPage);
    }
  }
}
