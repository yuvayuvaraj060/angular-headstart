import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AdminRoutingModule } from "./admin-routing.module";
import { LayoutsModule } from "src/app/core/layouts.module";

@NgModule({
  declarations: [AdminRoutingModule.component],
  imports: [CommonModule, AdminRoutingModule, LayoutsModule],
})
export class AdminModule {}
